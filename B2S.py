#function to convert the binary number to decimal
def bin2dec(a):
    #convert binary to int
    bin2int=int(a)
    #print the binary number
    print("This is the binary number:",a)
    #this will be the decimal number 
    decimal = 0
    #for loop, start at 0 and end when it reachs the size of the number
    for power in range(0,len(a)):
       #mod the binary so if there is a remandier aka if there is a 1 there then it will converted
        c= bin2int % 10
        #divide the binary by ten so it can move to the next number
        bin2int= int(bin2int/10)
        #make the decimal, add each c*2^n to get the decimal
        decimal+= c*2**power
    #print the decimal number    
    print("Here is the decimal number:",decimal)
    

#call and run the fuction 
bin2dec("100011101111")